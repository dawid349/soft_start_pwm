
#include "stm32f4xx_hal.h"



volatile static uint8_t DutyCycle=0;  //current duty cycle
volatile uint16_t delay=0;		//auxiliary  variable to set proper frequency
extern TIM_HandleTypeDef htim5;






static void setting_duty_cycle(TIM_HandleTypeDef *timer, uint8_t channel)
{
	switch(channel)
		{
		case 1:
			{
				timer->Instance->CCR1 = DutyCycle; //write duty cycle to proper timer register
			}
		break;
		case 2:
			{
				timer->Instance->CCR2 = DutyCycle;
			}
		break;
		case 3:
			{
				timer->Instance->CCR3 = DutyCycle;
			}
		break;
		case 4:
			{
				timer->Instance->CCR4 = DutyCycle;
			}
		}

}




static void softStart_init(TIM_HandleTypeDef *chosen_timer,uint8_t chosen_channel_PWM)
{
	DutyCycle=0;			//reset current DutyCycle


	switch(chosen_channel_PWM) //switching PWM channel
	{
	case 1:
		{
			HAL_TIM_PWM_Start(chosen_timer,TIM_CHANNEL_1); //start PWM on chosen channel
		}
	break;
	case 2:
		{
			HAL_TIM_PWM_Start(chosen_timer,TIM_CHANNEL_2);
		}
	break;
	case 3:
		{
			HAL_TIM_PWM_Start(chosen_timer,TIM_CHANNEL_3);
		}
	break;
	case 4:
		{
			HAL_TIM_PWM_Start(chosen_timer,TIM_CHANNEL_4);
		}
	break;
	}


}



void softStart_inCallback(TIM_HandleTypeDef *chosen_timer,uint8_t chosen_channel_PWM)
{
	 	 delay++;	//variable enabling frequency division by 1250 (5s to max PWM)

	 if(delay>1250)
	 	 {
		 	 delay=0;
		 	 DutyCycle++;	//increasing DutyCycle
		 	 setting_duty_cycle(chosen_timer,chosen_channel_PWM); //settting our DutyCycle

	 	 }
	 if(DutyCycle>199)	// when reach max PWM...
	 	 {
		 	 DutyCycle=199; //setting max PWM
		 	 setting_duty_cycle(chosen_timer,chosen_channel_PWM);
		 	 HAL_TIM_Base_Stop_IT(chosen_timer); //stop interrupts from timer, end of the softStart
	 	 }

}

void fan_init(TIM_HandleTypeDef *chosen_timer,uint8_t chosen_channel_PWM)
	{
		softStart_init(chosen_timer,chosen_channel_PWM);
	}
void fan_start(TIM_HandleTypeDef *chosen_timer)
	{
		HAL_TIM_Base_Start_IT(chosen_timer); //interrupts from timer overflow initialised
	}
void fan_stop(TIM_HandleTypeDef *timer, uint8_t channel)
	{
		DutyCycle=0;
		setting_duty_cycle(timer,channel);

	}



//timer overflow
//PWM frequency is 50kHz/2=25kHz (phase-correct PWM)
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) //default interrupt definition
{

 if(htim->Instance == TIM5){	//check if the interrupt flag comes from proper timer
	 softStart_inCallback(&htim5,1); //calling soft start function
	  }

}


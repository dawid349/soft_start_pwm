



#ifndef SOFTSTART_H_
#define SOFTSTART_H_

#include "stm32f4xx_hal.h"



void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
void softStart_inCallback(TIM_HandleTypeDef *chosen_timer,uint8_t chosen_channel_PWM);
void fan_init(TIM_HandleTypeDef *chosen_timer,uint8_t chosen_channel_PWM);
void fan_start(TIM_HandleTypeDef *chosen_timer);
void fan_stop(TIM_HandleTypeDef *timer, uint8_t channel);

/*function softSatrt_inCallback as the name suggest should be call in interrupts from any timer, here we have 200 interrupts per second
 * which give us increasing duty cycle of control signal from 0 - max_PWM in 5 seconds
 */




#endif /* SOFTSTART_H_*/
